# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_26_114038) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "address_verifications", force: :cascade do |t|
    t.integer "user_id"
    t.integer "country_id"
    t.string "city"
    t.string "state"
    t.string "street"
    t.string "apartment_number"
    t.integer "status", default: 0, null: false
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "addresses", force: :cascade do |t|
    t.integer "user_id"
    t.string "address"
    t.string "country"
    t.string "governorate"
    t.string "city"
    t.string "street"
    t.boolean "default_address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admin_logins", force: :cascade do |t|
    t.integer "admin_id"
    t.string "ip_address"
    t.string "user_agent"
    t.string "device_id"
    t.string "operation_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_admin_logins_on_admin_id"
  end

  create_table "admin_watchdogs", force: :cascade do |t|
    t.integer "admin_id"
    t.datetime "logintime"
    t.text "ipaddress"
    t.datetime "lastvisit"
    t.string "operation_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string "uuid"
    t.integer "roleid", default: 1
    t.string "username"
    t.string "firstname"
    t.string "lastname"
    t.string "language"
    t.boolean "disabled", default: false, null: false
    t.integer "loginattempts", default: 0, null: false
    t.boolean "status", default: false, null: false
    t.integer "failedattempts", default: 0, null: false
    t.string "secret_code"
    t.string "otp_secret_key"
    t.integer "active_otp", default: 1, null: false
    t.string "account_number"
    t.string "telephone"
    t.boolean "account_currency", default: false
    t.integer "country_id"
    t.string "invitation_code"
    t.string "unlock_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "encrypted_password", limit: 128
    t.string "confirmation_token", limit: 128
    t.string "remember_token", limit: 128
    t.index ["email"], name: "index_admins_on_email"
    t.index ["remember_token"], name: "index_admins_on_remember_token"
  end

  create_table "affilate_programs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "refered_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audit_logs", force: :cascade do |t|
    t.integer "user_id"
    t.string "user_name"
    t.string "trace_id"
    t.string "action_type"
    t.string "action_meta"
    t.string "ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.integer "user_id"
    t.string "bank_name"
    t.string "branche_name"
    t.string "swift_code"
    t.string "country"
    t.string "account_name"
    t.string "account_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards", force: :cascade do |t|
    t.string "number"
    t.integer "value"
    t.integer "status"
    t.date "expired_at"
    t.integer "user_id"
    t.integer "invoice_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "operation_id"
  end

  create_table "cards_categories", force: :cascade do |t|
    t.integer "card_value"
    t.integer "active"
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards_logs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "action_type"
    t.string "ip"
    t.integer "card_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_banks", force: :cascade do |t|
    t.string "bank_key"
    t.string "bank_name"
    t.string "encryptkey"
    t.string "currency"
    t.string "country"
    t.string "city"
    t.string "branch"
    t.string "branch_code"
    t.string "phone"
    t.string "account_name"
    t.string "account_email"
    t.string "account_number"
    t.string "swiftcode"
    t.string "ibancode"
    t.integer "visa_cvv"
    t.string "bank_category"
    t.string "bank_subcategory"
    t.float "fees"
    t.float "ratio"
    t.string "logo"
    t.date "expire_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
  end

  create_table "countries", force: :cascade do |t|
    t.string "short_code"
    t.string "Full_Name"
    t.string "Phone_code", limit: 10
    t.string "Currency"
    t.string "language"
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logins", force: :cascade do |t|
    t.integer "user_id"
    t.string "ip_address"
    t.string "user_agent"
    t.string "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "operation_type"
    t.index ["user_id"], name: "index_logins_on_user_id"
  end

  create_table "money_ops", force: :cascade do |t|
    t.string "opid"
    t.integer "optype"
    t.float "amount", limit: 53
    t.string "payment_gateway"
    t.integer "status"
    t.datetime "payment_date"
    t.string "payment_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "fees", limit: 53
  end

  create_table "nationalid_verifications", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "legal_name"
    t.string "national_id"
    t.integer "document_type"
    t.date "issue_date"
    t.date "expire_date"
    t.integer "status", default: 0
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.integer "notification_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications_settings", force: :cascade do |t|
    t.integer "user_id"
    t.integer "money_transactions"
    t.integer "pending_transactions"
    t.integer "transactions_updates"
    t.integer "help_tickets_updates"
    t.integer "tickets_replies"
    t.integer "account_login"
    t.integer "change_password"
    t.integer "verifications_setting"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "selfie_verifications", force: :cascade do |t|
    t.integer "user_id"
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0, null: false
  end

  create_table "sms_logs", force: :cascade do |t|
    t.integer "user_id"
    t.string "pinid"
    t.boolean "status", default: false
    t.integer "sms_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ticket_departments", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ticket_replies", force: :cascade do |t|
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "ticket_id"
    t.index ["ticket_id"], name: "index_ticket_replies_on_ticket_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "number"
    t.string "title"
    t.text "content"
    t.text "attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "responded", default: 0, null: false
  end

  create_table "uploads", force: :cascade do |t|
    t.text "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "user_infos", force: :cascade do |t|
    t.integer "user_id"
    t.integer "mobile"
    t.integer "address_verification_id"
    t.integer "nationalid_verification_id"
    t.integer "selfie_verification_id"
    t.integer "status", default: 0, null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_wallets", force: :cascade do |t|
    t.string "currency"
    t.float "amount", limit: 53
    t.integer "user_id"
    t.integer "status"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "uuid"
    t.integer "roleid", default: 2, null: false
    t.string "username"
    t.string "firstname"
    t.string "lastname"
    t.string "language"
    t.integer "disabled", default: 0, null: false
    t.integer "loginattempts", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "encrypted_password", limit: 128
    t.string "confirmation_token", limit: 128
    t.string "remember_token", limit: 128
    t.integer "failedattempts", default: 0, null: false
    t.string "auth_token"
    t.string "secret_code"
    t.string "otp_secret_key"
    t.integer "active_otp", default: 1
    t.string "account_number"
    t.string "telephone"
    t.boolean "account_currency", default: false
    t.integer "country_id"
    t.string "invitation_code"
    t.string "unlock_token"
    t.integer "shipping_adreess_id", default: 0
    t.index ["email"], name: "index_users_on_email"
    t.index ["remember_token"], name: "index_users_on_remember_token"
  end

  create_table "users_wallets_transfers", force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_to_id"
    t.integer "transfer_method"
    t.integer "transfer_type"
    t.integer "hold_period"
    t.float "amount", limit: 53
    t.float "ratio", limit: 53
    t.integer "approve"
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "verifications", force: :cascade do |t|
    t.integer "user_id"
    t.text "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wallets_transfer_ratios", force: :cascade do |t|
    t.string "key"
    t.float "value", limit: 53
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "watchdogs", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "logintime"
    t.text "ipaddress"
    t.datetime "lastvisit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "operation_type"
  end

end
