# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Country.create([{
#     short_code: "EGY",
#     Full_Name: "Egypt",
#     Phone_code: "002",
#     Currency:  "EGP",
#     language: "Arabic",
#     active: "1",
#             },
# {
#     short_code: "SAU",
#     Full_Name: "Saudi Arabia	",
#     Phone_code: "00966",
#     Currency:  "SAR",
#     language: "Arabic",
#     active: "1",
#             }
#         ])  

Admin.create([{
            email: "ahmedhassan20593@gmail.com",
            password: "Kh123456",
            username: "AHassan",
            firstname: "ahmad",
            lastname: "hasan",
            status: 1,
            account_number: "PS100000000",
            remember_token: Clearance::Token.new,
            country_id: 1
        },{
            email: "work@aymanfekri.com",
            password: "Ayman12345",
            username: "Ayman",
            firstname: "ayman",
            lastname: "fekri",
            status: 1,
            account_number: "PS100000011",
            remember_token: Clearance::Token.new,
            country_id: 1
        },{
            email: "omararansa10@gmail.com",
            password: "Omar12345",
            username: "Omar",
            firstname: "omar",
            lastname: "aransa",
            status: 1,
            account_number: "PS100000010",
            remember_token: Clearance::Token.new,
            country_id: 1
        },{
            email: "amira.elfayome@servicehigh.com",
            password: "Amira123456",
            username: "Amira",
            firstname: "amira",
            lastname: "elfayome",
            status: 1,
            account_number: "PS100000007",
            remember_token: Clearance::Token.new,
            country_id: 1
        }
])
