class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.integer :uuid
      t.integer :roleid
      t.string :username
      t.string :firstname
      t.string :lastname
      t.string :language
      t.integer :disabled, null: false, default: 0
      t.integer :loginattempts, null: false, default: 0
      t.integer :status, null: false, default: 0

      t.timestamps
    end
  end
end
