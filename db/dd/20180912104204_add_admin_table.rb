class AddAdminTable < ActiveRecord::Migration[5.2]
  def change
    create_table :admins do |t|
      t.string :uuid
      t.integer :roleid, default: 1
      t.string :username
      t.string :firstname
      t.string :lastname
      t.string :language
      t.boolean :disabled, null: false, default: false
      t.integer :loginattempts, null: false, default: 0
      t.boolean :status, null: false, default: false
      t.integer :failedattempts, null: false, default: 0
      t.string :secret_code
      t.string :otp_secret_key
      t.integer :active_otp, null: false, default: 1
      t.string :account_number
      t.string :telephone
      t.boolean :account_currency, default: false
      t.integer :country_id
      t.string :invitation_code
      t.string :unlock_token

      t.timestamps
    end
  end
end
