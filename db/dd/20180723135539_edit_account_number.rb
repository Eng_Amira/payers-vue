class EditAccountNumber < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :account_number, :string
  end
end
