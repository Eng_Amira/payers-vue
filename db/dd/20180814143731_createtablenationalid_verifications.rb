class CreatetablenationalidVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :nationalid_verifications do |t|
      t.integer :user_id, null: false
      t.string :legal_name
      t.integer :national_id
      t.integer :document_type
      t.date :issue_date
      t.date :expire_date
      t.integer :status, default: 0
      t.string :note

      t.timestamps
    end
  end
end
