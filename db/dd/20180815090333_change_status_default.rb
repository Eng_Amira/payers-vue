class ChangeStatusDefault < ActiveRecord::Migration[5.2]
  def change
    add_column :selfie_verifications, :status, :integer, null: false, :default => 0
    change_column :address_verifications, :status, :integer, null: false, :default => 0
  end
end
