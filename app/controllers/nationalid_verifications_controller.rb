class NationalidVerificationsController < ApplicationController
  before_action :set_nationalid_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # show list of all National Id Verifications,
  # @return [String] UserName
  # @return [String] Country
  # @return [String] City
  # @return [String] State
  # @return [String] Street
  # @return [String] apartment_number
  # @return [String] Status
  # @return [String] Note
  def index
    if current_user.roleid == 1 
         @nationalid_verifications = NationalidVerification.all
    else
         redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /nationalid_verifications/1
  # GET /nationalid_verifications/1.json
  def show
    if current_user.roleid != 1 and current_user.id != @nationalid_verification.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # edit National Id Verification
  # admin can edit national id verification only if status is not verified
  # @param [Integer] id
  def edit
    if (@nationalid_verification.status == "Verified") 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # edit National Id Verification,
  # only admins can edit status and note.
  # @param [String] legal_name
  # @param [Integer] national_id
  # @param [Integer] document_type
  # @param [Date] issue_date
  # @param [Date] expire_date
  # @param [Blob] attachment1
  # @param [Blob] attachment2
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [String] legal_name
  # @return [Integer] national_id
  # @return [Integer] document_type
  # @return [Date] issue_date
  # @return [Date] expire_date
  # @return [Blob] attachment1
  # @return [Blob] attachment2
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @nationalid_verification.update(nationalid_verification_params)
        if current_user.roleid == 1
          @userinfo = UserInfo.where("user_id =?", @nationalid_verification.user_id ).first
          if params[:nationalid_verification][:status] == "Verified"
             if @userinfo == nil              
                 UserInfo.create(:user_id => @nationalid_verification.user_id , :nationalid_verification_id => @nationalid_verification.id, :address_verification_id => 0, :selfie_verification_id => 0, :name => @nationalid_verification.legal_name, :status => "UnVerified")
             else
              @userinfo.update(:nationalid_verification_id => @nationalid_verification.id, :name => @nationalid_verification.legal_name)
              @user_address_verification = AddressVerification.where("user_id =?", @nationalid_verification.user_id ).first
              if @user_address_verification != nil and @user_address_verification.status == "Verified"
                 @user_selfie_verification = SelfieVerification.where("user_id =?", @nationalid_verification.user_id ).first
                 if @user_selfie_verification != nil and @user_selfie_verification.status == "Verified" and @userinfo.mobile == 1
                  @userinfo.update(:status => "Verified")
                 end
              end
             end
          elsif params[:nationalid_verification][:status] == "UnVerified" and @userinfo != nil
              @userinfo.update(:status => "UnVerified")
          end
        end
        format.html { redirect_to @nationalid_verification, notice: 'Nationalid verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @nationalid_verification }
        AuditLog.create(user_id: @nationalid_verification.user_id, user_name: current_user.uuid, action_type: "Update Nationalid verification status" , action_meta: "Nationalid verification status was successfully updated." , ip: request.env['REMOTE_ADDR'])
      else
        format.html { render :edit }
        format.json { render json: @nationalid_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nationalid_verification
      @nationalid_verification = NationalidVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nationalid_verification_params
      params.require(:nationalid_verification).permit(:user_id, :legal_name, :national_id, :document_type, :issue_date, :expire_date, :status, :note, :attachment1, :attachment2, images: [])
    end
end
