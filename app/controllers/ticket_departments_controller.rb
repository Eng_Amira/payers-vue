class TicketDepartmentsController < ApplicationController
  before_action :set_ticket_department, only: [:show, :edit, :update, :destroy]

  # GET list of all ticket departments and display it
  # @return [id] department unique ID (Created automatically).
  # @return [name] the department name.
  # @return [code] the department code.
  # @return [description] the department description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @ticket_departments = TicketDepartment.all
  end

  # GET a spacific ticket department and display it
  # @param  [Integer] id department unique ID.
  # @return [name] the department name.   
  # @return [code] the department code.
  # @return [description] the department description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new ticket department
  # @param [Integer] id department unique ID (Created automatically).
  # @param [String] name the department name.
  # @param [String] code the department code.
  # @param [String] description the department description.
  def new
    @ticket_department = TicketDepartment.new
  end

  # GET an existing ticket department and edit params
  # @param [String] name the department name.
  # @param [String] code the department code.
  # @param [String] description the department description.
  def edit
  end

  # POST a new ticket department and save it
  # @return [id] department unique ID (Created automatically).
  # @return [name] the department name.
  # @return [code] the department code.
  # @return [description] the department description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @ticket_department = TicketDepartment.new(ticket_department_params)

    respond_to do |format|
      if @ticket_department.save
        format.html { redirect_to @ticket_department, notice: 'Ticket department was successfully created.' }
        format.json { render :show, status: :created, location: @ticket_department }
      else
        format.html { render :new }
        format.json { render json: @ticket_department.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing ticket department params(name,code,description)
  # @return [id] department unique ID (Created automatically).
  # @return [name] the department name.
  # @return [code] the department code.
  # @return [description] the department description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @ticket_department.update(ticket_department_params)
        format.html { redirect_to @ticket_department, notice: 'Ticket department was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket_department }
      else
        format.html { render :edit }
        format.json { render json: @ticket_department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticket_departments/1
  # DELETE /ticket_departments/1.json
  def destroy
    if @ticket_department.destroy
      respond_to do |format|
        format.html { redirect_to ticket_departments_url, notice: 'Ticket department was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to ticket_departments_path, notice: "Ticket department can't be destroyed."}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket_department
      @ticket_department = TicketDepartment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_department_params
      params.require(:ticket_department).permit(:name, :code, :description)
    end
end
