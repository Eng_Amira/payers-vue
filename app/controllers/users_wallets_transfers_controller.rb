class UsersWalletsTransfersController < ApplicationController
  before_action :set_users_wallets_transfer, only: [:show, :edit, :update, :destroy]

  # GET /users_wallets_transfers
  # GET /users_wallets_transfers.json
  def index
    @users_wallets_transfers = UsersWalletsTransfer.all
  end

  # GET /users_wallets_transfers/1
  # GET /users_wallets_transfers/1.json
  def show
  end

  # GET /users_wallets_transfers/new
  def new
    @users_wallets_transfer = UsersWalletsTransfer.new
  end

  # GET /users_wallets_transfers/1/edit
  def edit
  end

  # POST /users_wallets_transfers
  # POST /users_wallets_transfers.json
  def create
    @users_wallets_transfer = UsersWalletsTransfer.new(users_wallets_transfer_params)
    @user1 = User.where("email = ? OR account_number = ?", params[:users_wallets_transfer][:user_id], params[:users_wallets_transfer][:user_id]).first
    @user = User.where("email = ? OR account_number = ?", params[:users_wallets_transfer][:user_to_id],params[:users_wallets_transfer][:user_to_id]).first
    @user_from = @user1.id
    @user_to = @user.id
    @users_wallets_transfer.user_id = @user1.id
    @users_wallets_transfer.user_to_id = @user.id
    @transfer_amount = @users_wallets_transfer.amount
    @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount,@users_wallets_transfer.hold_period)
    if @message == ""
      ActiveRecord::Base.transaction do

        @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
        @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
        @new_balance_from = @user_wallet_from.amount.to_f - @transfer_amount.to_f
        @new_balance_to = @user_wallet_to.amount.to_f + @transfer_amount.to_f
        @user_wallet_from.update(:amount => @new_balance_from )

        if @users_wallets_transfer.hold_period == "" || @users_wallets_transfer.hold_period.to_i == 0
          @users_wallets_transfer.approve = 1
          @user_wallet_to.update(:amount => @new_balance_to )
          @smstext = "#{@transfer_amount.to_f} USD has been successfully transferred to user #{@user.firstname} #{@user.lastname}"
          @smstext_to = "#{@transfer_amount.to_f} USD has been successfully added to your wallet by user #{@user1.firstname} #{@user1.lastname}"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => @user1.id.to_i)
        else
          @users_wallets_transfer.approve = 0
          @smstext = "#{@transfer_amount.to_f} USD transfer request to user #{@user.firstname} #{@user.lastname} has been successfully created and waiting the hold period"
          @smstext_to = "#{@transfer_amount.to_f} USD has been successfully added to you by user #{@user1.firstname} #{@user1.lastname} and waiting the hold period"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount ,:payment_gateway => "Payers" ,:status => 0,:payment_date => DateTime.now,:user_id => @user1.id.to_i)
        end

        if @users_wallets_transfer.transfer_type == "Service"
          @users_wallets_transfer.address_id = 0
        end
        
        AuditLog.create(user_id: @user1.id, user_name: current_user.uuid, action_type: "Balance transfer", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
        @users_wallets_transfer.operation_id = @money_operation.opid
        @notification = access_notification(@smstext,@smstext_to,@user,@user1)
        respond_to do |format|
          if @users_wallets_transfer.save
            format.html { redirect_to @users_wallets_transfer, notice: 'Users wallets transfer was successfully created.' }
            format.json { render :show, status: :created, location: @users_wallets_transfer }
          else
            format.html { render :new }
            format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
          end
        end
      end
    else
      redirect_to(new_users_wallets_transfer_path,:notice => @message )
    end
  end

  # PATCH/PUT /users_wallets_transfers/1
  # PATCH/PUT /users_wallets_transfers/1.json
  def update
    respond_to do |format|
      if @users_wallets_transfer.update(users_wallets_transfer_params)
        format.html { redirect_to @users_wallets_transfer, notice: 'Users wallets transfer was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_wallets_transfer }
      else
        format.html { render :edit }
        format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_wallets_transfers/1
  # DELETE /users_wallets_transfers/1.json
  def destroy
    @users_wallets_transfer.destroy
    respond_to do |format|
      format.html { redirect_to users_wallets_transfers_url, notice: 'Users wallets transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_wallets_transfer
      @users_wallets_transfer = UsersWalletsTransfer.find(params[:id])
    end

    def access_notification(smstext,smstext_to,user,user1)
      @user1 = user1
      @tel = @user1.telephone
      @user_mail = @user1.email
      @user_from = @user1.id.to_i
      @user = user
      @user_to = @user.id
      @smstext = smstext
      @smstext_to = smstext_to
      @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
      @user_notification_setting_to = NotificationsSetting.where(user_id: @user_to).first
      @aa = Notification.create(user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions)
      @bb = Notification.create(user_id: @user_to ,title: "Balance transfer", description: @smstext_to , notification_type: @user_notification_setting_to.money_transactions)

      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@tel,@smstext)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@tel,@smstext)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      end

      if @user_notification_setting_to.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@smstext_to)
      elsif @user_notification_setting_to.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to,:status => 1,:sms_type => 3)
      elsif @user_notification_setting_to.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@smstext_to)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_wallets_transfer_params
      params.require(:users_wallets_transfer).permit(:user_id, :user_to_id, :transfer_method, :transfer_type, :hold_period, :amount, :ratio, :approve, :note , :address_id)
    end
end
