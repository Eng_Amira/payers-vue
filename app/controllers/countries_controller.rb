class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :require_login, except: [:index]
  # show list of all countries,
  # return with details of all countries.
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  def index
    @countries = Country.all
  end

  # show details of the country
  # @param [Integer] id 
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  # @return [String] Phone_code
  # @return [String] language
  # @return [Integer] active
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
  end

  # create new Country, it works only if current user is an admin
  def new
    if current_user.roleid == 1
      @country = Country.new
    else
       redirect_to root_path , notice: "not allowed" 
    end
  end

  # edit Country, it works only if current user is an admin
  # @param [Integer] id
  def edit
    if current_user.roleid != 1
        redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new country
  # @param [String] short_code
  # @param [String] Full_Name
  # @param [String] Currency
  # @param [String] Phone_code
  # @param [String] language
  # @param [Integer] active
  # @return [Integer] id
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  # @return [String] Phone_code
  # @return [String] language
  # @return [Integer] active
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @country = Country.new(country_params)

    respond_to do |format|
      if @country.save
        format.html { redirect_to @country, notice: 'Country was successfully created.' }
        format.json { render :show, status: :created, location: @country }
      else
        format.html { render :new }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit country
  # @param [String] short_code
  # @param [String] Full_Name
  # @param [String] Currency
  # @param [String] Phone_code
  # @param [String] language
  # @param [Integer] active
  # @return [Integer] id
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  # @return [String] Phone_code
  # @return [String] language
  # @return [Integer] active
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @country.update(country_params)
        format.html { redirect_to @country, notice: 'Country was successfully updated.' }
        format.json { render :show, status: :ok, location: @country }
      else
        format.html { render :edit }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

   # delete country
   # @param [integer] id
  def destroy
    if current_user.roleid == 1
      @country.destroy
        respond_to do |format|
          format.html { redirect_to countries_url, notice: 'Country was successfully destroyed.' }
          format.json { head :no_content }
        end
    else
      redirect_to root_path , notice: "not allowed"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:short_code, :Full_Name, :Phone_code, :Currency, :language, :active)
    end
end
