module Api
  class TicketsController < ApplicationController
    before_action :set_ticket, only: [:show, :edit, :update, :destroy]
    skip_before_action :verify_authenticity_token
  
    # GET /tickets
    # GET /tickets.json
    def index
    # =begin
    # @api {get} /api/tickets 1-Request tickets List
    # @apiVersion 0.3.0
    # @apiName GetTicket
    # @apiGroup Tickets
    # @apiExample Example usage:
    # curl -i http://localhost:3000/api/tickets
    # @apiSuccess {Number} id Ticket unique ID.
    # @apiSuccess {string} number Ticket unique number.
    # @apiSuccess {string} title Ticket title.
    # @apiSuccess {string} content Ticket content.
    # @apiSuccess {string} attachment Ticket attachment.
    # @apiSuccess {Date} created_at  Date created.
    # @apiSuccess {Date} updated_at  Date Updated.
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # [
    #      {
    #         "id": 3,
    #         "number": "AS",
    #         "title": "Ticket 1",
    #         "content": "Content",
    #         "attachment": "Attachment",
    #         "created_at": "2018-09-08T12:38:16.000Z",
    #         "updated_at": "2018-09-08T12:38:16.000Z"
    #     },
    # ]
    # @apiError MissingToken invalid Token.
    # @apiErrorExample Error-Response:
    # HTTP/1.1 400 Bad Request
    #   {
    #     "error": "Missing token"
    #   }
    # =end
    @tickets = Ticket.all
        respond_to do |format|
        format.json { render json: @tickets }
    end
    end
  
    # GET /tickets/1
    # GET /tickets/1.json
    def show
      # =begin
        # @api {get} /api/tickets/{:id} 3-Request Specific Ticket
        # @apiVersion 0.3.0
        # @apiName GetSpecificTicket
        # @apiGroup Tickets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/tickets/1
        # @apiParam {Number} id Ticket ID.
        # @apiSuccess {Number} id Ticket unique ID.
        # @apiSuccess {string} number Ticket unique number.
        # @apiSuccess {string} title Ticket title.
        # @apiSuccess {string} content Ticket content.
        # @apiSuccess {string} attachment Ticket attachment.
        # @apiSuccess {Number} id Reply unique ID.
        # @apiSuccess {string} content Reply content.
        # @apiSuccess {string} ticket_id Ticket ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #      {
        #        "ticket": {
        #          "id": 3,
        #          "number": "AS",
        #          "title": "Ticket 1",
        #          "content": "Content",
        #          "attachment": "Attachment",
        #          "created_at": "2018-09-08T12:38:16.000Z",
        #          "updated_at": "2018-09-08T12:38:16.000Z"
        #      },
        #      "ticket_reply": [
        #      {
        #          "id": 1,
        #          "content": "content 1",
        #          "created_at": "2018-09-10T14:16:36.000Z",
        #          "updated_at": "2018-09-10T14:16:36.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 2,
        #          "content": "content 2",
        #          "created_at": "2018-09-10T14:18:18.000Z",
        #          "updated_at": "2018-09-10T14:18:18.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 3,
        #          "content": "content 3",
        #          "created_at": "2018-09-10T14:19:49.000Z",
        #          "updated_at": "2018-09-10T14:19:49.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 4,
        #          "content": "content 4",
        #          "created_at": "2018-09-10T14:24:52.000Z",
        #         "updated_at": "2018-09-10T14:24:52.000Z",
        #          "ticket_id": 3
        #      }
        #      ]
        #      }
        # @apiError TicketNotFound The id of the Ticket was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TicketNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
        @ticket_reply = TicketReply.where(:ticket_id => params[:id]).all
        respond_to do |format|
          format.json  { render :json => {:ticket => @ticket, :ticket_reply => @ticket_reply }}
        end
    end

    def showreplies
      # =begin
        # @api {get} /api/tickets/{:id}/replies 4-Request Specific Ticket replies
        # @apiVersion 0.3.0
        # @apiName GetSpecificTicketReplies
        # @apiGroup Tickets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/tickets/3/replies
        # @apiParam {Number} id Ticket ID.
        # @apiSuccess {Number} id Reply unique ID.
        # @apiSuccess {string} content Reply content.
        # @apiSuccess {string} ticket_id Ticket ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #      {
        #          "id": 1,
        #          "content": "content 1",
        #          "created_at": "2018-09-10T14:16:36.000Z",
        #          "updated_at": "2018-09-10T14:16:36.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 2,
        #          "content": "content 2",
        #          "created_at": "2018-09-10T14:18:18.000Z",
        #          "updated_at": "2018-09-10T14:18:18.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 3,
        #          "content": "content 3",
        #          "created_at": "2018-09-10T14:19:49.000Z",
        #          "updated_at": "2018-09-10T14:19:49.000Z",
        #          "ticket_id": 3
        #      },
        #      {
        #          "id": 4,
        #          "content": "content 4",
        #          "created_at": "2018-09-10T14:24:52.000Z",
        #         "updated_at": "2018-09-10T14:24:52.000Z",
        #          "ticket_id": 3
        #      }
        # @apiError TicketNotFound The id of the Ticket was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TicketNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      @ticket_reply = TicketReply.where(:ticket_id => params[:id]).all
      respond_to do |format|
        format.json { render json: @ticket_reply }
      end
    end

    
    
    def create_reply
      # =begin
        # @api {post} /api/tickets 5-Create a new ticket reply
        # @apiVersion 0.3.0
        # @apiName PostTicketReply
        # @apiGroup Tickets
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/tickets/reply \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "content": "content5",
        #   "ticket_id": 3
        # }'
        # @apiParam {string} content Reply content.
        # @apiParam {Number} ticket_id Ticket ID.

        # @apiSuccess {Number} id Reply unique ID.
        # @apiSuccess {string} content Reply content.
        # @apiSuccess {Number} ticket_id Ticket ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #          "id": 11,
        #          "content": "content5",
        #          "created_at": "2018-09-12T13:15:58.000Z",
        #          "updated_at": "2018-09-12T13:15:58.000Z",
        #          "ticket_id": 3
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      @ticket_reply = TicketReply.new(ticket_reply_params)
       @ticket_reply.save
       respond_to do |format|
        format.json { render json: @ticket_reply }
       end
    end

    # GET /tickets/new
    def new
      @ticket = Ticket.new
    end
  
    # GET /tickets/1/edit
    def edit
    end
  
    # POST /tickets
    # POST /tickets.json
    def create
      # =begin
        # @api {post} /api/tickets 2-Create a new ticket
        # @apiVersion 0.3.0
        # @apiName PostTicket
        # @apiGroup Tickets
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/tickets \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "number": "AB",
        #   "title": "Ticket 2",
        #   "content": "Content 2",
        #   "attachment": "Attachment 2"
        # }'
        # @apiParam {string} number Ticket unique number.
        # @apiParam {string} title Ticket title.
        # @apiParam {string} content Ticket content.
        # @apiParam {string} attachment Ticket attachment.

        # @apiSuccess {Number} id Ticket unique ID.
        # @apiSuccess {string} number Ticket unique number.
        # @apiSuccess {string} title Ticket title.
        # @apiSuccess {string} content Ticket content.
        # @apiSuccess {string} attachment Ticket attachment.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 4,
        #         "number": "AB",
        #         "title": "Ticket 2",
        #         "content": "Content 2",
        #         "attachment": "Attachment 2",
        #         "created_at": "2018-09-08T13:05:09.000Z",
        #         "updated_at": "2018-09-08T13:05:09.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      @ticket = Ticket.new(ticket_params)
  
      respond_to do |format|
        if @ticket.save
          format.json { render json: @ticket, status: :created, location: @ticket }
        else
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # PATCH/PUT /tickets/1
    # PATCH/PUT /tickets/1.json
    def update
      # =begin
        # @api {put} /api/tickets/{:id} 6-Update an ticket
        # @apiVersion 0.3.0
        # @apiName PutTicket
        # @apiGroup Tickets
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/tickets/6 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "title": "Ticket 3",
        #   "content": "Content 3",
        #   "attachment": "Attachment 3"
        # }'
        # @apiParam {string} title Ticket title.
        # @apiParam {string} content Ticket content.
        # @apiParam {string} attachment Ticket attachment.
       
        # @apiSuccess {Number} id Ticket unique ID.
        # @apiSuccess {string} number Ticket unique number.
        # @apiSuccess {string} title Ticket title.
        # @apiSuccess {string} content Ticket content.
        # @apiSuccess {string} attachment Ticket attachment.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 4,
        #         "title": "Ticket 3",
        #         "content": "Content 3",
        #         "attachment": "Attachment 3",
        #         "number": "AB",
        #         "created_at": "2018-09-08T13:05:09.000Z",
        #         "updated_at": "2018-09-08T13:10:00.000Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError TicketNotFound The id of the Ticket was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "TicketNotFound"
        #   }
        # =end


      respond_to do |format|
        if @ticket.update(ticket_params)
          format.json { render json: @ticket, status: :ok, location: @ticket }
        else
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      end
    end
  
    # DELETE /tickets/1
    # DELETE /tickets/1.json
    def destroy
      @ticket.destroy
      respond_to do |format|
        format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_ticket
        @ticket = Ticket.find(params[:id])
      end
  
      # Never trust parameters from the scary internet, only allow the white list through.
      def ticket_params
        params.require(:ticket).permit(:number, :title, :content, :attachment)
      end

      def ticket_reply_params
        params.permit(:content, :ticket_id)
      end
  end
end