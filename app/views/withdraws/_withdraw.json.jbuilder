json.extract! withdraw, :id, :user_id, :bank_id, :operation_id, :amount, :fees, :status, :withdraw_type, :note, :created_at, :updated_at
json.url withdraw_url(withdraw, format: :json)
