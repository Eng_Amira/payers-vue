json.extract! wallets_transfer_ratio, :id, :key, :value, :description, :created_at, :updated_at
json.url wallets_transfer_ratio_url(wallets_transfer_ratio, format: :json)
