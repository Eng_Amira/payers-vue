json.extract! affilate_program, :id, :user_id, :refered_by, :created_at, :updated_at
json.url affilate_program_url(affilate_program, format: :json)
