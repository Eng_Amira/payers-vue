json.extract! admin_watchdog, :id, :admin_id, :logintime, :ipaddress, :lastvisit, :operation_type, :created_at, :updated_at
json.url admin_watchdog_url(admin_watchdog, format: :json)
