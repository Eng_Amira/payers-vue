json.extract! cards_category, :id, :card_value, :active, :order, :created_at, :updated_at
json.url cards_category_url(cards_category, format: :json)
