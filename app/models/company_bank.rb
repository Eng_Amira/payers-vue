class CompanyBank < ApplicationRecord
    has_one_attached :logo
    delegate :filename, to: :logo, allow_nil: true
    before_save :check_default

    def check_default
        self.currency ||= "USD"
    end

end
