class UsersWalletsTransfer < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    enum transfer_method: { FromWallet: 1, FromVisa: 2 }
    enum transfer_type: { Product: 1, Service: 2 }
    # enum approve: { Pending: 0, Done: 1 }
end
