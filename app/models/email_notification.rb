class EmailNotification < ApplicationRecord

    # Manage EMail notification setting
    # @param [String] site_signature the sender email address(payers@payers.com).
    # @param [Integer] user_mail The receiver user email address.
    # @param [String] subject The subject of the email.
    # @param [String] text The email content text.
    # @param [String] EmailService_Provider The Email provider name.
    # @param [String] Localized_String The SMS sending language(user defualt language).
    def self.email_notification_setting(user_mail:'client@payers.com',subject:'Welcome To Payers',text:'welcome')

        # email_settings
        @site_signature = 'payers@payers.com'

        # content
        @user_mail = user_mail
        @subject = subject
        @text = text
        @EmailService_Provider = 'mailgun'     
        # @EmailTemplates = emailtemplate.

        # EmailContent
        @Localized_String = "english"

        EmailJob.perform_async(user_mail:@user_mail,subject:@subject,text:@text,via:@site_signature,email_provider:@EmailService_Provider)

    end

end