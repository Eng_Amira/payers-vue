class Withdraw < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    belongs_to :bank_account ,:foreign_key => "bank_id"
    validates :amount, presence: true
    validates :bank_id, presence: true
    validates :withdraw_type, presence: true

    def self.checkwithdraw(user_id,withdraw_amount)

        @user_wallet = UserWallet.where(user_id: user_id.to_i).first
               
        @message = ""

        if @user_wallet != nil
            if @user_wallet.amount.to_f < withdraw_amount.to_f
                @message  = @message + "This user doesn't have enough money for this operation"
            end

            if @user_wallet.status == 0
                @message  = @message + "Sorry, This user Wallet was Disabled"
            end
        else
            @message  = @message + "This User Wasn't Stored in Our Database"
        end

        return   @message 
    end
end
