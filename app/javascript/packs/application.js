/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb
import Vue from 'vue/dist/vue.esm'
import App from '../app.vue'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Autocomplete from 'v-autocomplete'
import 'v-autocomplete/dist/v-autocomplete.css'

Vue.use(Autocomplete)
Vue.use(BootstrapVue)

document.addEventListener('turbolinks:load',function() {
  var element = document.querySelector("#boards")
  if (element != undefined){
      const app = new Vue({
          el: element,
          data: {
              lists: JSON.parse(element.dataset.lists)
          },
          template: "<App :original_lists='lists' />",
          components: { App }
      })
  }
  
});