require 'test_helper'

class WatchdogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @watchdog = watchdogs(:one)
  end

  test "should get index" do
    get watchdogs_url
    assert_response :success
  end

  test "should get new" do
    get new_watchdog_url
    assert_response :success
  end

  test "should create watchdog" do
    assert_difference('Watchdog.count') do
      post watchdogs_url, params: { watchdog: { admin_id: @watchdog.admin_id, ipaddress: @watchdog.ipaddress, lastvisit: @watchdog.lastvisit, logintime: @watchdog.logintime } }
    end

    assert_redirected_to watchdog_url(Watchdog.last)
  end

  test "should show watchdog" do
    get watchdog_url(@watchdog)
    assert_response :success
  end

  test "should get edit" do
    get edit_watchdog_url(@watchdog)
    assert_response :success
  end

  test "should update watchdog" do
    patch watchdog_url(@watchdog), params: { watchdog: { admin_id: @watchdog.admin_id, ipaddress: @watchdog.ipaddress, lastvisit: @watchdog.lastvisit, logintime: @watchdog.logintime } }
    assert_redirected_to watchdog_url(@watchdog)
  end

  test "should destroy watchdog" do
    assert_difference('Watchdog.count', -1) do
      delete watchdog_url(@watchdog)
    end

    assert_redirected_to watchdogs_url
  end
end
