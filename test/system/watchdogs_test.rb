require "application_system_test_case"

class WatchdogsTest < ApplicationSystemTestCase
  setup do
    @watchdog = watchdogs(:one)
  end

  test "visiting the index" do
    visit watchdogs_url
    assert_selector "h1", text: "Watchdogs"
  end

  test "creating a Watchdog" do
    visit watchdogs_url
    click_on "New Watchdog"

    fill_in "Admin", with: @watchdog.admin_id
    fill_in "Ipaddress", with: @watchdog.ipaddress
    fill_in "Lastvisit", with: @watchdog.lastvisit
    fill_in "Logintime", with: @watchdog.logintime
    click_on "Create Watchdog"

    assert_text "Watchdog was successfully created"
    click_on "Back"
  end

  test "updating a Watchdog" do
    visit watchdogs_url
    click_on "Edit", match: :first

    fill_in "Admin", with: @watchdog.admin_id
    fill_in "Ipaddress", with: @watchdog.ipaddress
    fill_in "Lastvisit", with: @watchdog.lastvisit
    fill_in "Logintime", with: @watchdog.logintime
    click_on "Update Watchdog"

    assert_text "Watchdog was successfully updated"
    click_on "Back"
  end

  test "destroying a Watchdog" do
    visit watchdogs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Watchdog was successfully destroyed"
  end
end
