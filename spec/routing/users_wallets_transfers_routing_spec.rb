require "rails_helper"

RSpec.describe UsersWalletsTransfersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/users_wallets_transfers").to route_to("users_wallets_transfers#index")
    end

    it "routes to #new" do
      expect(:get => "/users_wallets_transfers/new").to route_to("users_wallets_transfers#new")
    end

    it "routes to #show" do
      expect(:get => "/users_wallets_transfers/1").to route_to("users_wallets_transfers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/users_wallets_transfers/1/edit").to route_to("users_wallets_transfers#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/users_wallets_transfers").to route_to("users_wallets_transfers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/users_wallets_transfers/1").to route_to("users_wallets_transfers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/users_wallets_transfers/1").to route_to("users_wallets_transfers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/users_wallets_transfers/1").to route_to("users_wallets_transfers#destroy", :id => "1")
    end
  end
end
