require 'rails_helper'

RSpec.describe "company_banks/index", type: :view do
  before(:each) do
    assign(:company_banks, [
      CompanyBank.create!(
        :bank_key => "Bank Key",
        :bank_name => "Bank Name",
        :encryptkey => "Encryptkey",
        :currency => "Currency",
        :country => "Country",
        :city => "City",
        :branch => "Branch",
        :branch_code => "Branch Code",
        :phone => "Phone",
        :account_name => "Account Name",
        :account_email => "Account Email",
        :account_number => "Account Number",
        :swiftcode => "Swiftcode",
        :ibancode => "Ibancode",
        :visa_cvv => 2,
        :bank_category => "Bank Category",
        :bank_subcategory => "Bank Subcategory",
        :fees => 3.5,
        :ratio => 4.5,
        :logo => "Logo",
        :expire_at => ""
      ),
      CompanyBank.create!(
        :bank_key => "Bank Key",
        :bank_name => "Bank Name",
        :encryptkey => "Encryptkey",
        :currency => "Currency",
        :country => "Country",
        :city => "City",
        :branch => "Branch",
        :branch_code => "Branch Code",
        :phone => "Phone",
        :account_name => "Account Name",
        :account_email => "Account Email",
        :account_number => "Account Number",
        :swiftcode => "Swiftcode",
        :ibancode => "Ibancode",
        :visa_cvv => 2,
        :bank_category => "Bank Category",
        :bank_subcategory => "Bank Subcategory",
        :fees => 3.5,
        :ratio => 4.5,
        :logo => "Logo",
        :expire_at => ""
      )
    ])
  end

  it "renders a list of company_banks" do
    render
    assert_select "tr>td", :text => "Bank Key".to_s, :count => 2
    assert_select "tr>td", :text => "Bank Name".to_s, :count => 2
    assert_select "tr>td", :text => "Encryptkey".to_s, :count => 2
    assert_select "tr>td", :text => "Currency".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Branch".to_s, :count => 2
    assert_select "tr>td", :text => "Branch Code".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Account Name".to_s, :count => 2
    assert_select "tr>td", :text => "Account Email".to_s, :count => 2
    assert_select "tr>td", :text => "Account Number".to_s, :count => 2
    assert_select "tr>td", :text => "Swiftcode".to_s, :count => 2
    assert_select "tr>td", :text => "Ibancode".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Bank Category".to_s, :count => 2
    assert_select "tr>td", :text => "Bank Subcategory".to_s, :count => 2
    assert_select "tr>td", :text => 3.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.5.to_s, :count => 2
    assert_select "tr>td", :text => "Logo".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
