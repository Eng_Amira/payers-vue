require 'rails_helper'

RSpec.describe "selfie_verifications/index", type: :view do
  before(:each) do
    assign(:selfie_verifications, [
      SelfieVerification.create!(
        :user_id => 2,
        :note => "Note"
      ),
      SelfieVerification.create!(
        :user_id => 2,
        :note => "Note"
      )
    ])
  end

  it "renders a list of selfie_verifications" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
