require 'rails_helper'

RSpec.describe "money_ops/index", type: :view do
  before(:each) do
    assign(:money_ops, [
      MoneyOp.create!(
        :opid => "Opid",
        :optype => 2,
        :amount => "Amount",
        :payment_gateway => 3,
        :status => 4,
        :payment_id => 5,
        :user_id => 6
      ),
      MoneyOp.create!(
        :opid => "Opid",
        :optype => 2,
        :amount => "Amount",
        :payment_gateway => 3,
        :status => 4,
        :payment_id => 5,
        :user_id => 6
      )
    ])
  end

  it "renders a list of money_ops" do
    render
    assert_select "tr>td", :text => "Opid".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Amount".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
  end
end
