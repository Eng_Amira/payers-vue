require 'rails_helper'

RSpec.describe "admin_logins/new", type: :view do
  before(:each) do
    assign(:admin_login, AdminLogin.new(
      :admin => nil,
      :ip_address => "MyString",
      :user_agent => "MyString",
      :device_id => "MyString",
      :operation_type => "MyString"
    ))
  end

  it "renders new admin_login form" do
    render

    assert_select "form[action=?][method=?]", admin_logins_path, "post" do

      assert_select "input[name=?]", "admin_login[admin_id]"

      assert_select "input[name=?]", "admin_login[ip_address]"

      assert_select "input[name=?]", "admin_login[user_agent]"

      assert_select "input[name=?]", "admin_login[device_id]"

      assert_select "input[name=?]", "admin_login[operation_type]"
    end
  end
end
