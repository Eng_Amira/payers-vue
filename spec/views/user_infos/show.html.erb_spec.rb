require 'rails_helper'

RSpec.describe "user_infos/show", type: :view do
  before(:each) do
    @user_info = assign(:user_info, UserInfo.create!(
      :user_id => 2,
      :mobile => "Mobile",
      :address_verification_id => 3,
      :nationalid_verification_id => 4,
      :nationalid_verification_id => 5,
      :selfie_verification => 6,
      :status => 7
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Mobile/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
  end
end
