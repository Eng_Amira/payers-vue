require 'rails_helper'

RSpec.describe "bank_accounts/new", type: :view do
  before(:each) do
    assign(:bank_account, BankAccount.new(
      :bank_name => "MyText",
      :branche_name => "MyText",
      :swift_code => "MyText",
      :country => "MyText",
      :account_name => "MyText",
      :account_number => "MyText"
    ))
  end

  it "renders new bank_account form" do
    render

    assert_select "form[action=?][method=?]", bank_accounts_path, "post" do

      assert_select "textarea[name=?]", "bank_account[bank_name]"

      assert_select "textarea[name=?]", "bank_account[branche_name]"

      assert_select "textarea[name=?]", "bank_account[swift_code]"

      assert_select "textarea[name=?]", "bank_account[country]"

      assert_select "textarea[name=?]", "bank_account[account_name]"

      assert_select "textarea[name=?]", "bank_account[account_number]"
    end
  end
end
