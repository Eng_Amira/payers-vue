require 'rails_helper'

RSpec.describe "address_verifications/edit", type: :view do
  before(:each) do
    @address_verification = assign(:address_verification, AddressVerification.create!(
      :user_id => 1,
      :country_id => 1,
      :city => "MyString",
      :state => "MyString",
      :street => "MyString",
      :building => "MyString",
      :number => "MyString",
      :status => 1,
      :note => "MyString"
    ))
  end

  it "renders the edit address_verification form" do
    render

    assert_select "form[action=?][method=?]", address_verification_path(@address_verification), "post" do

      assert_select "input[name=?]", "address_verification[user_id]"

      assert_select "input[name=?]", "address_verification[country_id]"

      assert_select "input[name=?]", "address_verification[city]"

      assert_select "input[name=?]", "address_verification[state]"

      assert_select "input[name=?]", "address_verification[street]"

      assert_select "input[name=?]", "address_verification[building]"

      assert_select "input[name=?]", "address_verification[number]"

      assert_select "input[name=?]", "address_verification[status]"

      assert_select "input[name=?]", "address_verification[note]"
    end
  end
end
