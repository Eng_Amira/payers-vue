require 'rails_helper'

RSpec.describe "ticket_departments/new", type: :view do
  before(:each) do
    assign(:ticket_department, TicketDepartment.new(
      :name => "MyString",
      :code => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new ticket_department form" do
    render

    assert_select "form[action=?][method=?]", ticket_departments_path, "post" do

      assert_select "input[name=?]", "ticket_department[name]"

      assert_select "input[name=?]", "ticket_department[code]"

      assert_select "input[name=?]", "ticket_department[description]"
    end
  end
end
