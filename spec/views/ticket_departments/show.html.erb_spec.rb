require 'rails_helper'

RSpec.describe "ticket_departments/show", type: :view do
  before(:each) do
    @ticket_department = assign(:ticket_department, TicketDepartment.create!(
      :name => "Name",
      :code => "Code",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/Description/)
  end
end
